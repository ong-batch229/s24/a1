// Exponent Operator

let x = 90
let y = 3
function cube(x, y){
	return x ** y
}
let getCube = cube(90, 3);

// Template Literals

let cubeSentence = `The cube of ${x} is ${getCube}`;
console.log(cubeSentence);

// Array Destructuring

const address = ["258", "Washington Ave NW", "California", "90011"];
let [houseNum, street, city, stateNum] = address;
console.log(`I live in ${houseNum} ${street}, ${city} ${stateNum}`);

// Object Destructuring

const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weiged at ${weight} with a measurement of ${measurement}.`)

// Arrow Functions

let numbers = [1, 2, 3, 4, 5];
let resultNumbers = numbers.forEach((num) => {
	console.log(num)
})
console.log(resultNumbers);

// Javascript Classes

class Dog {
	constructor(name, age, breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
}
let dog1 = new Dog("Bantay", 5, "Askal");
let dog2 = new Dog("Odin", 3, "German Shephard");
console.log(dog1);
console.log(dog2);